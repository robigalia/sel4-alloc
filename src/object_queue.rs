// Copyright 2016 Robigalia Project Developers
//
// Licensed under the Apache License, Version 2.0, <LICENSE-APACHE or
// http://apache.org/licenses/LICENSE-2.0> or the MIT license <LICENSE-MIT or
// http://opensource.org/licenses/MIT>, at your option. This file may not be
// copied, modified, or distributed except according to those terms.

use intrusive_collections::{LinkedList, UnsafeRef, linked_list};
use alloc::vec::Vec;
use alloc::boxed::Box;
use core::{mem, ptr};

/// An object managed by an `ObjectQueue`.
pub struct Object<T> {
    free_list: linked_list::Link,
    pub object: T,
}

impl<T> Object<T> {
    pub fn new(object: T) -> Object<T> {
        Object {
            free_list: Default::default(),
            object: object,
        }
    }

    pub fn is_linked(&self) -> bool {
        self.free_list.is_linked()
    }
}

/// Set of objects managed by the `ObjectQueue`.
pub struct ObjectSet<T, U> {
    set_list: linked_list::Link,
    pub objects: Vec<Object<T>>,
    pub metadata: U,
}

impl<T, U> ObjectSet<T, U> {
    pub fn new(objects: Vec<Object<T>>, metadata: U) -> ObjectSet<T, U> {
        ObjectSet {
            set_list: Default::default(),
            objects: objects,
            metadata: metadata,
        }
    }
}

intrusive_adapter!(pub FreeObjects<T> = UnsafeRef<Object<T>>:
                       Object<T> { free_list: linked_list::Link });
intrusive_adapter!(pub ObjectSets<T, U> = UnsafeRef<ObjectSet<T, U>>:
                       ObjectSet<T, U> { set_list: linked_list::Link });

/// Simple free-list object queue
///
/// The object queue manages sets of objects, which are contained Vecs. Individual objects are
/// threaded through a free list. Requesting or freeing a new object is an O(1) operation.
///
/// It is very important for safety that objects managed by a queue are not returned as free
/// objects to another queue, and that the objects themselves never outlive this queue. Otherwise,
/// deallocating the `ObjectQueue` will cause dangling pointers in all objects which were sourced
/// from this queue.
///
/// For this reason, the destructor of `ObjectQueue` leaks all object sets. In a debug build, it
/// will also panic. Releasing the resources  `ObjectQueue` should be done with the (unsafe)
/// `free_resources` method.
pub struct ObjectQueue<T, U> {
    sets: LinkedList<ObjectSets<T, U>>,
    free: LinkedList<FreeObjects<T>>,
    num_objects_loaned: usize,
}

impl<T, U> Default for ObjectQueue<T, U> {
    fn default() -> Self {
        ObjectQueue {
            sets: LinkedList::new(ObjectSets(Default::default())),
            free: LinkedList::new(FreeObjects(Default::default())),
            num_objects_loaned: 0,
        }
    }
}

impl<T, U> Drop for ObjectQueue<T, U> {
    fn drop(&mut self) {
        panic!("this will leak all resources in a release build and shouldn't be dropped")
    }
}

impl<T, U> ObjectQueue<T, U> {
    pub fn new() -> Self {
        Default::default()
    }

    /// Free all `ObjectSet`s tracked by this queue.
    ///
    /// This is unsafe if there are any objects currently loaned from the queue. In debug builds,
    /// this will be tracked, and will panic if there are objects loaned.
    pub unsafe fn free_resources(mut self) {
        debug_assert_eq!(self.num_objects_loaned, 0);

        {
            let mut c = self.sets.cursor_mut();

            while !c.is_null() {
                let mut set = UnsafeRef::into_box(c.remove().unwrap());
                ptr::drop_in_place(&mut set.objects);
                ptr::drop_in_place(&mut set);
            }
        }

        mem::forget(self);
    }

    /// Direct access to the inner linked list of sets.
    pub unsafe fn sets_mut(&mut self) -> &mut LinkedList<ObjectSets<T, U>> {
        &mut self.sets
    }

    /// Direct access to the inner linked list of free objects.
    pub unsafe fn free_mut(&mut self) -> &mut LinkedList<FreeObjects<T>> {
        &mut self.free
    }

    /// Direct access to the inner linked list of free objects and sets.
    pub unsafe fn free_and_sets_mut(
        &mut self,
    ) -> (&mut LinkedList<FreeObjects<T>>, &mut LinkedList<ObjectSets<T, U>>) {
        let &mut ObjectQueue {
            ref mut free,
            ref mut sets,
            ..
        } = self;

        (free, sets)
    }

    /// Add an `ObjectSet` to this queue.
    pub fn add_object_set(&mut self, set: ObjectSet<T, U>) {
        let mut c = self.free.cursor_mut();

        for obj in &set.objects {
            c.insert_before(unsafe { UnsafeRef::from_raw(obj) });
        }

        self.sets.cursor_mut().insert_after(
            UnsafeRef::from_box(Box::new(set)),
        );
    }

    /// Return an object to the queue.
    ///
    /// # Safety
    ///
    /// If the object is not owned by this queue, undefined behavior will result.
    pub unsafe fn return_object(&mut self, object: UnsafeRef<Object<T>>) {
        self.free.cursor_mut().insert_after(object);

        if cfg!(debug) {
            self.num_objects_loaned.wrapping_sub(1);
        }
    }

    /// Get an object from the queue.
    pub fn get_object(&mut self) -> Option<UnsafeRef<Object<T>>> {
        let val = self.free.front_mut().remove();

        if cfg!(debug) && val.is_some() {
            self.num_objects_loaned.wrapping_add(1);
        }

        val
    }
}
