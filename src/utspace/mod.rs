// Copyright 2016 Robigalia Project Developers
//
// Licensed under the Apache License, Version 2.0, <LICENSE-APACHE or
// http://apache.org/licenses/LICENSE-2.0> or the MIT license <LICENSE-MIT or
// http://opensource.org/licenses/MIT>, at your option. This file may not be
// copied, modified, or distributed except according to those terms.

use core::cell::Cell;

use sel4::{self, SlotRef, ToCap, Window};
use sel4_sys::seL4_Untyped_Retype;

/// Untyped region tracking.
///
/// Tracks how many objects have been allocated in an untyped region, as well as its size and the
/// number of remaining bytes.
///
/// This roughly mirrors the kernel's internal state about untyped objects, and is useful for
/// knowing whether object allocation into an untyped will be possible. All other utspace
/// allocators come down to managing these buckets.
///
/// Note that if deallocation is done without calling `deallocate`, tracking will become
/// desynchronized with the kernel and memory will be wasted.
///
/// Similarly, if allocation is done without informing this object, it will think there is more
/// free memory than there actually is.
#[derive(Debug)]
pub struct UtBucket {
    cap: SlotRef,
    objects: Cell<usize>,
    bytes_used: Cell<usize>,
    total_bytes: usize,
}

impl UtBucket {
    /// Create a new `UtBucket` given a reference to an untyped memory capability and the size_bits
    /// it was created with.
    pub fn new(cap: SlotRef, size_bits: u8) -> UtBucket {
        UtBucket {
            cap: cap,
            objects: Cell::new(0),
            bytes_used: Cell::new(0),
            total_bytes: 1 << (size_bits as usize),
        }
    }

    pub fn has_space(&self, window: Window, size_bits: usize) -> bool {
        let bytes_needed = window.num_slots * size_bits;
        let bytes_used = self.bytes_used.get();

        !(bytes_used + (bytes_used % bytes_needed) + bytes_needed > self.total_bytes)
    }

    /// Mark `count` objects as deleted.
    ///
    /// If the object count becomes 0, revoke the untyped capability, ensuring it has no children
    /// and can be reused. The return value is the attempt of that revoke, if there was one.
    fn delete(&self, count: usize) -> Option<sel4::Result> {
        self.objects.set(self.objects.get() - count);
        if self.objects.get() == 0 {
            match self.cap.revoke() {
                c @ Ok(_) => {
                    self.bytes_used.set(0);
                    Some(c)
                },
                c => Some(c),
            }
        } else {
            None
        }
    }
}

impl ::UTSpaceManager for UtBucket {
    fn allocate<T: sel4::Allocatable>(
        &self,
        dest: sel4::Window,
        size_bits: usize,
    ) -> Result<(), (usize, sel4::Error)> {
        let size_in_bytes = T::object_size(size_bits) as usize;
        #[cfg(feature = "debug")]
        println!(
            "Doing an Allocable::create with dest {:?}, size bits {}, size in bytes {}",
            dest,
            size_bits,
            size_in_bytes
        );

        let res = T::create(self.cap.cptr, dest, size_bits);

        match res {
            Ok(_) => {
                self.bytes_used.set(
                    self.bytes_used.get() + (self.bytes_used.get() % size_in_bytes) +
                        dest.num_slots * size_in_bytes,
                );
                self.objects.set(self.objects.get() + dest.num_slots);
                Ok(())
            },
            Err(e) => Err((0, e)),
        }
    }

    fn allocate_raw(
        &self,
        dest: Window,
        size_bits: usize,
        objtype: usize,
    ) -> Result<(), (usize, sel4::Error)> {
        let size_in_bytes = 1 << size_bits;

        #[cfg(feature = "debug")]
        println!(
            "Doing an UntypedReptype with dest {:?}, size bits {}, objtype {}, size in bytes {}",
            dest,
            size_bits,
            objtype,
            size_in_bytes
        );

        let res = unsafe {
            seL4_Untyped_Retype(
                self.cap.cptr,
                objtype,
                size_bits,
                dest.cnode.root.to_cap(),
                dest.cnode.cptr,
                dest.cnode.depth as usize,
                dest.first_slot_idx,
                dest.num_slots,
            )
        };

        if res == 0 {
            self.bytes_used.set(
                self.bytes_used.get() + (self.bytes_used.get() % size_in_bytes) +
                    dest.num_slots * size_in_bytes,
            );
            self.objects.set(self.objects.get() + dest.num_slots);
            Ok(())
        } else {
            Err((
                0,
                sel4::Error(sel4::GoOn::CheckIPCBuf {
                    error_code: res,
                }),
            ))
        }
    }

    fn deallocate_raw(&self, window: Window, _size_bits: usize) -> sel4::Result {
        match self.delete(window.num_slots) {
            Some(e) => e,
            None => Ok(()),
        }
    }

    fn minimum_slots(&self) -> usize {
        0
    }

    fn minimum_untyped(&self) -> usize {
        0
    }

    fn minimum_vspace(&self) -> usize {
        0
    }
}
