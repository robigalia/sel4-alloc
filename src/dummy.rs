// Copyright 2016 Robigalia Project Developers
//
// Licensed under the Apache License, Version 2.0, <LICENSE-APACHE or
// http://apache.org/licenses/LICENSE-2.0> or the MIT license <LICENSE-MIT or
// http://opensource.org/licenses/MIT>, at your option. This file may not be
// copied, modified, or distributed except according to those terms.

use alloc::vec::Vec;

use sel4_sys::{seL4_CPtr, seL4_CapRights};
use sel4;

use {AllocatorBundle, BulkCSpaceManager, CSpaceManager, UTSpaceManager, VSpaceManager,
     seL4_ARCH_VMAttributes};

#[derive(Copy, Clone, Debug)]
pub struct DummyAlloc;

#[allow(unused_variables)]
impl VSpaceManager for DummyAlloc {
    type Reservation = (usize, usize);

    fn map<A: AllocatorBundle>(
        &self,
        _: &[seL4_CPtr],
        _: u8,
        _: seL4_CapRights,
        _: seL4_ARCH_VMAttributes,
        _: &A,
    ) -> usize {
        unimplemented!()
    }

    fn map_at_vaddr<A: AllocatorBundle>(
        &self,
        _: &[seL4_CPtr],
        _: usize,
        _: u8,
        _: &Self::Reservation,
        _: seL4_CapRights,
        _: seL4_ARCH_VMAttributes,
        _: &A,
    ) -> bool {
        unimplemented!()
    }

    fn change_protection<A: AllocatorBundle>(
        &self,
        _: usize,
        _: usize,
        _: seL4_CapRights,
        _: seL4_ARCH_VMAttributes,
        _: &A,
    ) {
        unimplemented!()
    }

    fn unmap(&self, _: usize, _: usize, _: Option<&mut Vec<seL4_CPtr>>) {
        unimplemented!()
    }

    fn reserve<A: AllocatorBundle>(&self, _: usize, _: &A) -> Option<Self::Reservation> {
        unimplemented!()
    }

    fn reserve_at_vaddr<A: AllocatorBundle>(
        &self,
        _: usize,
        _: usize,
        _: &A,
    ) -> Option<Self::Reservation> {
        unimplemented!()
    }

    fn unreserve<A: AllocatorBundle>(&self, _: Self::Reservation, _: &A) {
        unimplemented!()
    }

    fn unreserve_at_vaddr<A: AllocatorBundle>(&self, _: usize, _: &A) -> Result<(), ()> {
        unimplemented!()
    }

    fn get_cap(&self, _: usize) -> Option<seL4_CPtr> {
        unimplemented!()
    }

    fn root(&self) -> seL4_CPtr {
        unimplemented!()
    }

    fn minimum_slots(&self) -> usize {
        unimplemented!()
    }

    fn minimum_untyped(&self) -> usize {
        unimplemented!()
    }

    fn minimum_vspace(&self) -> usize {
        unimplemented!()
    }
}

#[allow(unused_variables)]
impl CSpaceManager for DummyAlloc {
    fn allocate_slot<A: AllocatorBundle>(&self, _: &A) -> Result<seL4_CPtr, ()> {
        unimplemented!()
    }

    fn free_slot<A: AllocatorBundle>(&self, _: seL4_CPtr, _: &A) -> Result<(), ()> {
        unimplemented!()
    }

    fn slot_info(&self, _: seL4_CPtr) -> Option<&sel4::CNodeInfo> {
        unimplemented!()
    }

    fn slot_window(&self, _: seL4_CPtr) -> Option<sel4::Window> {
        unimplemented!()
    }

    fn minimum_slots(&self) -> usize {
        unimplemented!()
    }

    fn minimum_untyped(&self) -> usize {
        unimplemented!()
    }

    fn minimum_vspace(&self) -> usize {
        unimplemented!()
    }
}

#[allow(unused_variables)]
impl BulkCSpaceManager for DummyAlloc {
    type Token = &'static sel4::Window;

    fn allocate_slots<A: AllocatorBundle>(&self, _: usize, _: &A) -> Result<Self::Token, ()> {
        unimplemented!()
    }

    fn free_slots<A: AllocatorBundle>(&self, _: Self::Token, _: &A) -> Result<(), ()> {
        unimplemented!()
    }

    fn slots_info(&self, _: &Self::Token) -> Option<&sel4::CNodeInfo> {
        unimplemented!()
    }
}

#[allow(unused_variables)]
impl UTSpaceManager for DummyAlloc {
    fn allocate<T: sel4::Allocatable>(
        &self,
        _: sel4::Window,
        _: usize,
    ) -> Result<(), (usize, sel4::Error)> {
        unimplemented!()
    }

    fn deallocate<T: sel4::Allocatable>(
        &self,
        window: sel4::Window,
        size_bits: usize,
    ) -> sel4::Result {
        self.deallocate_raw(window, T::object_size(size_bits) as usize)
    }

    fn allocate_raw(
        &self,
        _: sel4::Window,
        _: usize,
        _: usize,
    ) -> Result<(), (usize, sel4::Error)> {
        unimplemented!()
    }

    fn deallocate_raw(&self, _: sel4::Window, _: usize) -> sel4::Result {
        unimplemented!()
    }

    fn minimum_slots(&self) -> usize {
        unimplemented!()
    }

    fn minimum_untyped(&self) -> usize {
        unimplemented!()
    }

    fn minimum_vspace(&self) -> usize {
        unimplemented!()
    }
}
