// Copyright 2016 Robigalia Project Developers
//
// Licensed under the Apache License, Version 2.0, <LICENSE-APACHE or
// http://apache.org/licenses/LICENSE-2.0> or the MIT license <LICENSE-MIT or
// http://opensource.org/licenses/MIT>, at your option. This file may not be
// copied, modified, or distributed except according to those terms.

//! Hierarchical virtual address space handling.
//!
//! This module assumes a particular flavor of paging structures. The paging structures are
//! hierarchical, with each level being able to store pages that are some power-of-two size. All
//! page sizes on level N are strictly greater than all page sizes on level N-1. An entry at level
//! N can store either a page of any size from level N, or a table whose entries are at level N-1.
//!
//! For example, x86 with PAE could be described as:
//!
//!  - Level 2: 4KiB, "PTE"
//!  - Level 1: 2MiB, "PDE"
//!  - Level 0: (no pages, only tables) "PDPT"
//!
//! ARMv7 could be described as:
//!
//!  - Level 2: 4KiB, 64KiB (pages)
//!  - Level 1: 1MiB, 16MiB (sections)
//!  - Level 0: (no pages, only tables)

use core::{fmt, mem};
use core::cell::{Cell, RefCell};

use sel4_sys::*;
use intrusive_collections::{rbtree, RBTree, Bound, UnsafeRef, KeyAdapter, IntrusivePointer};
use alloc::vec::Vec;
use alloc::boxed::Box;
use alloc::heap::Global;
use alloc::allocator::{Alloc, Layout};

use {AllocatorBundle, CSpaceManager, UTSpaceManager, VSpaceManager, VSpaceReservation,
     seL4_ARCH_VMAttributes};

// HIER_LEVELS is (objtype,         // seL4 object type constant for our page table object
//                 level_sizes,     // log2 size of page frames that can be directly mapped here
//                 bits_translated, // number of bits to use as an index into our table
//                 size_bits)       // log2 size of our page table object
//                Index 0 is the root page table object
// MAP_PAGE: sel4_sys function that maps a page frame for this architecture
// UNMAP_PAGE: sel4_sys function that unmaps a page frame for this architecture
// MAP_FNS is (table_map_fn,        // sel4_sys function to map next lower level page table
//             table_unmap_fn)      // sel4_sys function to unmap next lower level page table
//            Index 0 is the root page table object
//            None if not applicable
// VIRT_ADDR_BITS: Number of bits in a virtual address translated by these tables
// VADDR_LIMIT: The start of kernel-protected virtual address space

pub type MapFn = unsafe fn(seL4_CPtr, seL4_CPtr, usize, seL4_CapRights, seL4_ARCH_VMAttributes)
                           -> isize;
pub type RemapFn = unsafe fn(seL4_CPtr, seL4_CPtr, seL4_CapRights, seL4_ARCH_VMAttributes) -> isize;
pub type UnmapFn = unsafe fn(seL4_CPtr) -> isize;

#[cfg(all(target_arch = "arm", target_pointer_width = "32"))]
mod arch {
    use sel4_sys::*;
    use super::{MapFn, RemapFn, UnmapFn};
    pub type TblMapFn = unsafe fn(seL4_CPtr, seL4_CPtr, usize, seL4_ARM_VMAttributes) -> isize;
    pub const HIER_LEVELS: [(isize, [u8; 2], u8, u8); 2] =
        [
            (seL4_ARM_PageDirectoryObject as isize, [20, 24], 8, 14),
            (seL4_ARM_PageTableObject as isize, [12, 18], 12, 10),
        ];
    pub const MAP_PAGE: MapFn = seL4_ARM_Page_Map;
    pub const REMAP_PAGE: RemapFn = seL4_ARM_Page_Remap as RemapFn;
    pub const UNMAP_PAGE: UnmapFn = seL4_ARM_Page_Unmap;
    pub const MAP_FNS: [Option<(TblMapFn, UnmapFn)>; 2] =
        [
            Some((seL4_ARM_PageTable_Map as TblMapFn, seL4_ARM_PageTable_Unmap as UnmapFn)),
            None,
        ];
    pub const VADDR_BITS: u8 = 32;
    pub const VADDR_LIMIT: usize = 0xe0000000; // TODO: This depends on specific platform
    pub const BITS: u8 = 32;
}

#[cfg(all(target_arch = "x86", target_pointer_width = "32"))]
mod arch {
    use sel4_sys::*;
    use super::{MapFn, RemapFn, UnmapFn};

    // Note: no PAE
    pub type TblMapFn = unsafe fn(seL4_CPtr, seL4_CPtr, usize, seL4_X86_VMAttributes) -> isize;

    pub const HIER_LEVELS: [(isize, [u8; 1], u8, u8); 2] =
        [
            (seL4_X86_PageDirectoryObject as isize, [22], 10, 12),
            (seL4_X86_PageTableObject as isize, [12], 10, 12),
        ];
    pub const MAP_PAGE: MapFn = seL4_X86_Page_Map as MapFn;
    pub const REMAP_PAGE: RemapFn = seL4_X86_Page_Remap as RemapFn;
    pub const UNMAP_PAGE: UnmapFn = seL4_X86_Page_Unmap;
    pub const MAP_FNS: [Option<(TblMapFn, UnmapFn)>; 2] =
        [
            Some((seL4_X86_PageTable_Map as TblMapFn, seL4_X86_PageTable_Unmap as UnmapFn)),
            None,
        ];
    pub const VADDR_BITS: u8 = 32;
    pub const VADDR_LIMIT: usize = 0xe0000000;
    pub const BITS: u8 = 32;
}

#[cfg(target_arch = "x86_64")]
mod arch {
    use sel4_config::*;
    use sel4_sys::*;
    use super::{MapFn, RemapFn, UnmapFn};

    pub type TblMapFn = unsafe fn(seL4_CPtr, seL4_CPtr, usize, seL4_X86_VMAttributes) -> isize;

    pub const HIER_LEVELS: [(isize, [u8; 1], u8, u8); 4] =
        [
            (seL4_X64_PML4Object as isize, [0], 9, 12),
            (seL4_X86_PDPTObject as isize, [30], 9, 12),
            (seL4_X86_PageDirectoryObject as isize, [21], 9, 12),
            (seL4_X86_PageTableObject as isize, [12], 9, 12),
        ];
    pub const MAP_PAGE: MapFn = seL4_X86_Page_Map as MapFn;
    pub const REMAP_PAGE: RemapFn = seL4_X86_Page_Remap as RemapFn;
    pub const UNMAP_PAGE: UnmapFn = seL4_X86_Page_Unmap;
    pub const MAP_FNS: [Option<(TblMapFn, UnmapFn)>; 4] =
        [
            Some((seL4_X86_PDPT_Map as TblMapFn, seL4_X86_PDPT_Unmap as UnmapFn)),
            Some((seL4_X86_PageDirectory_Map as TblMapFn, seL4_X86_PageDirectory_Unmap as UnmapFn)),
            Some((seL4_X86_PageTable_Map as TblMapFn, seL4_X86_PageTable_Unmap as UnmapFn)),
            None,
        ];
    pub const VADDR_BITS: u8 = 48;
    #[cfg(not(feature = "CONFIGX_ENABLE_SMP_SUPPORT"))]
    pub const VADDR_LIMIT: usize = 0xff80_00000000;
    #[cfg(feature = "CONFIGX_ENABLE_SMP_SUPPORT")]
    // PPTR_BASE - TLBBITMAP_ROOT_ENTRIES * BIT(PML4_INDEX_OFFSET)
    pub const VADDR_LIMIT: usize = 0xff80_00000000 -
        (((CONFIG_MAX_NUM_NODES - 1) / ((1 << 6) - 1)) + 1) * (1 << 39);
    pub const BITS: u8 = 64;
}

use self::arch::*;

/// This is the header which tracks information about a level in the paging hierarchy.
pub struct LevelNode {
    /// Cap for this table, passed to MAP_FNS[self.depth].1
    table_cap: seL4_CPtr,
    /// Index into HIER_LEVELS and MAP_FNS.
    depth: u8,
    /// Number of entries in this table.
    log2_size: u8,
}

#[derive(Clone, Copy)]
enum LevelEntry {
    Table(*mut LevelNode),
    Page {
        cap: seL4_CPtr,
        rights: seL4_CapRights,
        attrs: seL4_ARCH_VMAttributes,
    },
    Free,
}

fn get_n_bits_at(word: usize, n: u8, start: u8) -> usize {
    debug_assert!(n <= VADDR_BITS);
    debug_assert!(start < VADDR_BITS);
    debug_assert!(VADDR_BITS <= 64); // y'know, just in case.
    debug_assert!(VADDR_BITS <= BITS);
    (word >> start as usize) & (!0 >> ((BITS - n) as usize % BITS as usize))
}

/// Create a new, empty level for use at `depth` in the paging hierarchy.
fn new_level<A: AllocatorBundle>(alloc: &A, depth: u8, table_cap: Option<seL4_CPtr>) -> &LevelNode {
    let cptr;

    match table_cap {
        Some(cap) => cptr = cap,
        None => {
            cptr = alloc.cspace().allocate_slot(alloc).unwrap();
            alloc
                .utspace()
                .allocate_raw(
                    alloc.cspace().slot_window(cptr).unwrap(),
                    HIER_LEVELS[depth as usize].3 as usize,
                    HIER_LEVELS[depth as usize].0 as usize,
                )
                .unwrap();
        },
    }

    let ptr = unsafe {
        Global.alloc_zeroed(
            Layout::from_size_align(
                LevelNode::total_size(HIER_LEVELS[depth as usize].2),
                mem::align_of::<LevelNode>(),
            ).unwrap(),
        ).unwrap().as_ptr() as *mut LevelNode
    };

    assert!(!ptr.is_null());

    let obj = unsafe { &mut *(ptr as *mut LevelNode) };
    obj.table_cap = cptr;
    obj.depth = depth;
    obj.log2_size = HIER_LEVELS[depth as usize].2;

    let tb = obj.get_table_pointer();

    for i in 0..1 << obj.log2_size {
        unsafe { *tb.offset(i) = LevelEntry::Free }
    }

    unsafe { &*(obj as *const LevelNode) }
}

impl LevelNode {
    /// Calculate the number of bytes used to store this level, including the header, alignment,
    /// and table after it.
    fn total_size(log2_size: u8) -> usize {
        let mut sz = 0;
        sz += mem::size_of::<LevelNode>();
        sz += mem::align_of::<LevelEntry>() - sz % mem::align_of::<LevelEntry>();
        sz += (1 << log2_size as usize) * mem::size_of::<LevelEntry>();
        sz
    }

    /// Get a pointer to the first entry in the table.
    fn get_table_pointer(&self) -> *mut LevelEntry {
        let mut ptr = self as *const _ as usize;
        ptr += mem::size_of::<LevelNode>();
        ptr += mem::align_of::<LevelEntry>() - ptr % mem::align_of::<LevelEntry>();
        debug_assert_eq!(ptr % mem::align_of::<LevelEntry>(), 0);
        ptr as *mut LevelEntry
    }

    /// Given a virtual address and the number of bits already translated, return the index into
    /// this level of the paging hierarchy which should be used to continue lookup.
    fn get_level_index(&self, vaddr: usize, bits_translated: usize) -> usize {
        let n = HIER_LEVELS[self.depth as usize].2;
        get_n_bits_at(vaddr, n, VADDR_BITS - bits_translated as u8 - n)
    }

    /// Walk the paging hierarchy, calling back for each level which influences the lookup of
    /// `vaddr`.
    ///
    /// If the callback returns `None`, walking continues. Otherwise, it returns the return value
    /// of the callback. If the entire table is traversed without the callback returning `Some`,
    /// `None` is returned.
    fn walk_table<T, F: FnMut(u8, *mut LevelEntry) -> Option<T>>(
        &self,
        vaddr: usize,
        mut f: F,
    ) -> Option<T> {
        let mut cur: &'static _ = unsafe { &*(self as *const LevelNode) };
        let mut bits_translated = 0;
        let mut depth = 0;

        loop {
            let tp = cur.get_table_pointer();
            let n = HIER_LEVELS[depth as usize].2;
            let tbidx = get_n_bits_at(vaddr, n, VADDR_BITS - bits_translated as u8 - n);
            let ptr = unsafe { tp.offset(tbidx as isize) };

            if let c @ Some(_) = f(depth, ptr) {
                return c;
            }

            match unsafe { *ptr } {
                LevelEntry::Table(p) => {
                    cur = unsafe { &*(p as *const LevelNode) };
                },
                _ => return None,
            }

            bits_translated += n;
            depth += 1;
        }
    }

    /// Walk the paging hierarchy, calling back for each `LevelEntry` in a given range of
    /// addresses.
    ///
    /// If the callback returns `None`, walking continues. Otherwise, it returns the return value
    /// of the callback. If the entire range is traversed without the callback returning `Some`,
    /// `None` is returned.
    fn walk_table_range<T, F: FnMut(u8, usize, *const LevelNode, *mut LevelEntry) -> Option<T>>(
        &self,
        start: usize,
        end: usize,
        mut f: F,
    ) -> Option<T> {
        // vaddr is sign-extended to the full 64-bit range for addresses >= 0x800000000000
        // mask off the high bits to ignore that.
        self.walk_table_range_inner(
            start & (!0 >> (BITS as usize - VADDR_BITS as usize)),
            (end & (!0 >> (BITS as usize - VADDR_BITS as usize))) - 1,
            0,
            0,
            &mut f,
        )
    }

    fn walk_table_range_inner<T,
                              F: FnMut(u8, usize, *const LevelNode, *mut LevelEntry) -> Option<T>>
        (&self,
         target_start: usize,
         target_end: usize,
         table_start: usize,
         bits_translated: usize,
         f: &mut F)
-> Option<T>{
        let cur: &'static _ = unsafe { &*(self as *const LevelNode) };
        // table_end = table start + num table entries * virt memory used by 1 entry in table - 1
        //             (the -1 is to prevent overflow)
        let table_end = (table_start +
                             (1 << HIER_LEVELS[cur.depth as usize].2) *
                                 (1 <<
                                      (VADDR_BITS - bits_translated as u8 -
                                           (HIER_LEVELS[cur.depth as usize].2)))) -
            1;
        // If the start or end address isn't inside the virtual address space covered by our node
        // then we're somewhere in the middle of the target range and we need to traverse every
        // entry.
        let start_lvidx = if target_start >= table_start && target_start <= table_end {
            self.get_level_index(target_start, bits_translated)
        } else {
            0
        };
        let end_lvidx = if target_end >= table_start && target_end <= table_end {
            self.get_level_index(target_end, bits_translated) + 1
        } else {
            1 << HIER_LEVELS[cur.depth as usize].2
        };
        let tp = cur.get_table_pointer();
        for i in start_lvidx..end_lvidx {
            let ptr = unsafe { tp.offset(i as isize) };
            if let c @ Some(_) = f(
                cur.depth,
                table_start +
                    i * // table_start + i * vm_used_by_table_entry
                        (1 << (VADDR_BITS - bits_translated as u8 -
                        (HIER_LEVELS[cur.depth as usize].2))),
                cur,
                ptr,
            )
            {
                return c;
            }
            if let LevelEntry::Table(p) = unsafe { *ptr } {
                let p: &'static _ = unsafe { &*(p as *const LevelNode) };
                if let c @ Some(_) = p.walk_table_range_inner(
                    target_start,
                    target_end,
                    table_start +
                        i *
                            (1 <<
                                 (VADDR_BITS - bits_translated as u8 -
                                      (HIER_LEVELS[cur.depth as usize].2))),
                    bits_translated +
                        (HIER_LEVELS[cur.depth as usize].2 as usize),
                    f,
                )
                {
                    return c;
                }
            }
        }
        None
    }

    fn get_level_entry(&self, vaddr: usize) -> Option<&LevelEntry> {
        self.walk_table(vaddr, |_, entry| match unsafe { *entry } {
            LevelEntry::Page {
                ..
            } => Some(unsafe { &*(entry as *const LevelEntry) }),
            _ => None,
        })
    }
}

pub struct ReservationNode {
    num_bytes: Cell<usize>,
    start_addr: Cell<usize>,
    address_link: rbtree::Link,
    size_link: rbtree::Link,
}

impl ReservationNode {
    fn split(&self, num_bytes_to_take: usize) -> UnsafeRef<ReservationNode> {
        UnsafeRef::from_box(Box::new(ReservationNode {
            num_bytes: Cell::new(self.num_bytes.get() - num_bytes_to_take),
            start_addr: Cell::new(self.start_addr.get() + num_bytes_to_take),
            address_link: Default::default(),
            size_link: Default::default(),
        }))
    }
}

impl ReservationNode {
    fn end(&self) -> usize {
        self.start_addr.get() + self.num_bytes.get()
    }
}

intrusive_adapter!(SizeOrder = UnsafeRef<ReservationNode>:
                       ReservationNode { size_link: rbtree::Link });
intrusive_adapter!(AddressOrder = UnsafeRef<ReservationNode>:
                       ReservationNode { address_link: rbtree::Link });

impl<'a> KeyAdapter<'a> for SizeOrder {
    type Key = usize;
    fn get_key(&self, container: &'a ReservationNode) -> usize {
        container.num_bytes.get()
    }
}

impl<'a> KeyAdapter<'a> for AddressOrder {
    type Key = usize;
    fn get_key(&self, container: &'a ReservationNode) -> usize {
        container.start_addr.get()
    }
}

pub struct Hier {
    free_blocks: RefCell<RBTree<SizeOrder>>,
    all_blocks: RefCell<RBTree<AddressOrder>>,
    top_level: *mut LevelNode,
}


impl fmt::Debug for Hier {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        let mut saved_from_address = 0;
        let mut saved_to_address = 0;
        let mut saved_rights = seL4_CapRights::new(0, 0, 0);
        let mut saved_attrs: seL4_ARCH_VMAttributes = Default::default();
        let mut saved_depth = 0;
        let mut saved_pages = 0;
        let _ = write!(f, "Reservations:\n");
        let all = self.all_blocks.borrow();
        for block in all.iter() {
            let isfree = block.size_link.is_linked();
            let _ = write!(f,
                     "  {} from {:12x} to {:12x}\n",
                     if isfree { "    free" } else { "RESERVED"},
                     block.start_addr.get(),
                     block.start_addr.get() + block.num_bytes.get());
        }
        let _ = write!(f, "Mappings:\n");
        let _ = write!(f, "VSpace Root cptr={}\n", unsafe { &(*self.top_level).table_cap });
        unsafe { &*self.top_level }.walk_table_range(
            0,
            usize::max_value(),
            |depth,
             level_entry_vaddr,
             _,
             level_entry| {
                match unsafe { *level_entry } {
                    LevelEntry::Free => {
                        if saved_pages > 0 {
                            fmt_page_info(
                                f,
                                saved_pages,
                                saved_from_address,
                                saved_to_address,
                                saved_rights,
                                saved_attrs,
                                saved_depth,
                            );
                            saved_pages = 0;
                        }
                    },
                    LevelEntry::Table(_) => {
                        if saved_pages > 0 {
                            fmt_page_info(
                                f,
                                saved_pages,
                                saved_from_address,
                                saved_to_address,
                                saved_rights,
                                saved_attrs,
                                saved_depth,
                            );
                            saved_pages = 0;
                        }
                        for _ in 0..depth {
                            let _ = f.write_str("      ");
                        }
                        let bits: u8 = (0..depth + 1).map(|x| HIER_LEVELS[x as usize].2).sum();
                        let _ = write!(f,
                           "PageTable --------{:->12x}--to--{:->12x}--------------------\n",
                           level_entry_vaddr,
                           level_entry_vaddr + (1 << (VADDR_BITS - bits)));
                    },
                    LevelEntry::Page {
                        rights,
                        attrs,
                        ..
                    } => {
                        if saved_pages > 0 &&
                            (depth != saved_depth || rights != saved_rights ||
                                 attrs != saved_attrs)
                        {
                            fmt_page_info(
                                f,
                                saved_pages,
                                saved_from_address,
                                saved_to_address,
                                saved_rights,
                                saved_attrs,
                                saved_depth,
                            );
                            saved_pages = 0;
                        }
                        if saved_pages == 0 {
                            saved_from_address = level_entry_vaddr;
                            saved_rights = rights;
                            saved_attrs = attrs;
                            saved_depth = depth;
                        }
                        saved_pages += 1;
                        let bits: u8 = (0..depth + 1).map(|x| HIER_LEVELS[x as usize].2).sum();
                        saved_to_address = level_entry_vaddr + (1 << (VADDR_BITS - bits));
                    },
                }

                None::<()>
            },
        );
        if saved_pages > 0 {
            fmt_page_info(
                f,
                saved_pages,
                saved_from_address,
                saved_to_address,
                saved_rights,
                saved_attrs,
                saved_depth,
            );
        }
        Ok(())
    }
}

fn fmt_page_info(
    f: &mut fmt::Formatter,
    pages: usize,
    from: usize,
    to: usize,
    rights: seL4_CapRights,
    attrs: seL4_ARCH_VMAttributes,
    level: u8,
) {
    for _ in 0..level {
        let _ = f.write_str("      ");
    }
    let _ = write!(f,
                   "{:4} Pages  {:12x}  to  {:12x}  rights={}{}{} attrs={}\n",
                   pages,
                   from,
                   to,
                   if rights.get_capAllowGrant() == 1 { "g" } else { "-" },
                   if rights.get_capAllowRead() == 1 { "r" } else { "-" },
                   if rights.get_capAllowWrite() == 1 { "w" } else { "-" },
                   attrs as usize);
}

impl Hier {
    pub fn new<A: AllocatorBundle>(alloc: &A, vspace: Option<seL4_CPtr>) -> Hier {
        let nl = new_level(alloc, 0, vspace);
        let hier = Hier::from_toplevel(nl as *const LevelNode as *mut LevelNode);
        hier.init_block_lists();
        hier
    }

    pub fn from_toplevel(top_level: *mut LevelNode) -> Hier {
        Hier {
            free_blocks: RefCell::new(Default::default()),
            all_blocks: RefCell::new(Default::default()),
            top_level: top_level,
        }
    }

    fn init_block_lists(&self) {
        let mut free = self.free_blocks.borrow_mut();
        let mut all = self.all_blocks.borrow_mut();

        let new_entry = UnsafeRef::from_box(Box::new(ReservationNode {
            num_bytes: Cell::new(VADDR_LIMIT),
            start_addr: Cell::new(0),
            address_link: Default::default(),
            size_link: Default::default(),
        }));

        all.insert(new_entry.clone());
        free.insert(new_entry);
    }

    /// "Mock" mapping pages into the VSpace, updating internal bookkeeping but not modifying the
    /// paging structures at all.
    ///
    /// This is useful during bootstrap or to otherwise record actions on the vspace that did not
    /// occur via this manager.
    pub fn mock_map_at_vaddr<A: AllocatorBundle>(
        &self,
        caps: &[seL4_CPtr],
        vaddr: usize,
        size_bits: u8,
        res: &Reservation,
        rights: seL4_CapRights,
        attrs: seL4_ARCH_VMAttributes,
        alloc: &A,
    ) -> bool {
        if vaddr < res.res().start_addr.get() ||
            (vaddr + (1 << size_bits) * caps.len()) > res.res().end()
        {
            return false;
        }
        self.map_pages_at_vaddr_raw(caps, vaddr, rights, size_bits, attrs, alloc, true);
        true
    }

    /// "Mock" unmapping pages into the VSpace, updating internal bookkeeping but not modifying the
    /// paging structures at all.
    ///
    /// This is useful during bootstrap or to otherwise record actions on the vspace that did not
    /// occur via this manager.
    pub fn mock_unmap(&self, vaddr: usize, bytes: usize, caps: Option<&mut Vec<seL4_CPtr>>) {
        self.unmap_raw(vaddr, bytes, caps, true)
    }

    pub fn mock_change_protection<A: AllocatorBundle>(
        &self,
        vaddr: usize,
        bytes: usize,
        rights: seL4_CapRights,
        attrs: seL4_ARCH_VMAttributes,
        alloc: &A,
    ) {
        self.change_protection_raw(vaddr, bytes, rights, attrs, alloc, true);
    }

    fn map_pages_at_vaddr_raw<A: AllocatorBundle>(
        &self,
        caps: &[seL4_CPtr],
        vaddr: usize,
        rights: seL4_CapRights,
        size_bits: u8,
        attrs: seL4_ARCH_VMAttributes,
        alloc: &A,
        mock: bool,
    ) {
        assert_eq!(vaddr & (!0 >> (BITS - size_bits)), 0, "vaddr not aligned");
        assert!(size_bits > 0);
        let page_size_bytes = 1 << size_bits;
        let total_mapping_bytes = page_size_bytes * caps.len();
        let vaddr_end = vaddr + total_mapping_bytes;
        assert!(vaddr_end > vaddr, "vaddr overflow");
        let vroot = unsafe { &*self.top_level }.table_cap;
        let mut cap_index = 0;
        let mut vaddr_cur = vaddr;

        unsafe { &*self.top_level }.walk_table_range(vaddr, vaddr_end, |depth,
                                                                        level_entry_vaddr,
                                                                        _,
                                                                        level_entry| {
            match unsafe { *level_entry } {
                LevelEntry::Free => {
                    if HIER_LEVELS[depth as usize].1.contains(&size_bits) {
                        // map at this level
                        assert!(cap_index < caps.len());
                        assert!(vaddr_cur < vaddr_end);
                        assert_eq!(vaddr_cur & (!0 >> (BITS as usize - VADDR_BITS as usize)),
                                   level_entry_vaddr);
                        unsafe {
                            *level_entry = LevelEntry::Page {
                                cap: caps[cap_index],
                                rights: rights,
                                attrs: attrs,
                            }
                        };
                        if !mock {
                            #[cfg(feature = "debug")]
                            println!("Mapping page at level {} at vaddr {:x}", depth, vaddr_cur);
                            let res = unsafe {
                                MAP_PAGE(caps[cap_index],
                                         vroot,
                                         vaddr_cur,
                                         rights,
                                         attrs)
                            };
                            assert_eq!(res, 0);
                        }
                        cap_index += 1;
                        vaddr_cur += page_size_bytes;
                    } else {
                        // create a page table
                        assert!(MAP_FNS[depth as usize].is_some());
                        let tb = new_level(alloc, depth + 1, None);
                        unsafe {
                            *level_entry = LevelEntry::Table(tb as *const _ as *mut LevelNode)
                        };
                        if !mock {
                            let res = unsafe {
                                #[cfg(feature = "debug")]
                                println!("Mapping page table level {} at vaddr {:x} for page at\
                                         vaddr {:x}", depth, level_entry_vaddr, vaddr_cur);
                                MAP_FNS[depth as usize].unwrap().0((*tb).table_cap,
                                                                   vroot,
                                                                   vaddr_cur,
                                                                   Default::default())
                            };
                            assert_eq!(res, 0);
                        }
                    }
                },
                LevelEntry::Table(_) => {
                    assert!(!HIER_LEVELS[depth as usize].1.contains(&size_bits),
                        // we're mapping at this level, entry should be free
                        "Unexepected page table found at vaddr {:x} at level {}
                            while attempting to map page index {} of size {} at vaddr {:x}
                            while mapping {} pages at vaddrs from {:x} to {:x}",
                        level_entry_vaddr,
                        depth,
                        cap_index,
                        page_size_bytes,
                        vaddr_cur,
                        caps.len(),
                        vaddr,
                        vaddr_end
                    );
                },
                LevelEntry::Page { .. } => {
                    panic!("Unexepected page found at vaddr {:x} at level {}
                               while attempting to map page index {} of size {} at vaddr {:x}
                               while mapping {} pages at vaddrs from {:x} to {:x}",
                           level_entry_vaddr,
                           depth,
                           cap_index,
                           page_size_bytes,
                           vaddr_cur,
                           caps.len(),
                           vaddr,
                           vaddr_end);
                },
            }
            None::<()>
        });

        assert_eq!(cap_index, caps.len());
        assert_eq!(vaddr_cur, vaddr_end);
    }

    fn unmap_raw(
        &self,
        vaddr: usize,
        bytes: usize,
        mut caps: Option<&mut Vec<seL4_CPtr>>,
        mock: bool,
    ) {
        let bytes = bytes + bytes % 4096;

        unsafe { &*self.top_level }.walk_table_range(
            vaddr,
            vaddr + bytes,
            |_, _, _, level_entry| {
                if let LevelEntry::Page {
                    cap, ..
                } = unsafe { *level_entry }
                {
                    if !mock {
                        unsafe { UNMAP_PAGE(cap) };
                    }

                    if let Some(ref mut v) = caps {
                        v.push(cap);
                    }

                    unsafe {
                        *level_entry = LevelEntry::Free;
                    }
                }

                None::<()>
            },
        );
    }

    fn change_protection_raw<A: AllocatorBundle>(
        &self,
        vaddr: usize,
        bytes: usize,
        rights: seL4_CapRights,
        attrs: seL4_ARCH_VMAttributes,
        _: &A,
        mock: bool,
    ) {
        let bytes = bytes + bytes % 4096;
        let vroot = unsafe { &*self.top_level }.table_cap;

        unsafe { &*self.top_level }.walk_table_range(
            vaddr,
            vaddr + bytes,
            |_, _, _, level_entry| {
                if let LevelEntry::Page {
                    cap, ..
                } = unsafe { *level_entry }
                {
                    unsafe {
                        if !mock {
                            let res = REMAP_PAGE(cap, vroot, rights, attrs);
                            assert_eq!(res, 0);
                        }
                        *level_entry = LevelEntry::Page {
                            cap,
                            rights,
                            attrs,
                        }
                    }
                }

                None::<()>
            },
        );
    }
}

pub struct Reservation {
    reservation: UnsafeRef<ReservationNode>,
}

impl Reservation {
    fn res(&self) -> &ReservationNode {
        &*self.reservation
    }
}

impl VSpaceReservation for Reservation {
    fn start_vaddr(&self) -> usize {
        let mut vaddr = self.res().start_addr.get();
        // vaddr is sign-extended to the full 64-bit range for addresses >= 0x800000000000
        if VADDR_BITS < BITS && (vaddr & (1 << (VADDR_BITS - 1))) != 0 {
            vaddr |= ((!0 as u64) << VADDR_BITS) as usize;
        }
        vaddr
    }
    fn end_vaddr(&self) -> usize {
        let mut vaddr = self.res().end();
        // vaddr is sign-extended to the full 64-bit range for addresses >= 0x800000000000
        if VADDR_BITS < BITS && (vaddr & (1 << (VADDR_BITS - 1))) != 0 {
            vaddr |= ((!0 as u64) << VADDR_BITS) as usize;
        }
        vaddr
    }
}

impl Hier {
    fn coalesce(&self, block: &ReservationNode) {
        let mut all = self.all_blocks.borrow_mut();
        let mut free = self.free_blocks.borrow_mut();
        let mut start_addr = block.start_addr.get();
        let mut num_bytes = block.num_bytes.get();

        // coalesce forward, absorbing free ranges by adding their num_pages to our own.
        unsafe {
            let mut fwd = all.cursor_mut_from_ptr(block);
            fwd.move_next();

            while !fwd.is_null() && fwd.get().unwrap().size_link.is_linked() {
                let val = UnsafeRef::into_box(fwd.remove().unwrap());
                num_bytes += val.num_bytes.get();
                free.cursor_mut_from_ptr(&*val).remove();
                drop(val);
            }
        };

        // coalesce backward, absorbing free ranges by extending our start_addr.
        unsafe {
            let mut bkd = all.cursor_mut_from_ptr(block);
            bkd.move_prev();

            while !bkd.is_null() && bkd.get().unwrap().size_link.is_linked() {
                let val = UnsafeRef::into_box(bkd.remove().unwrap());
                start_addr = val.start_addr.get();
                num_bytes += val.num_bytes.get();
                free.cursor_mut_from_ptr(&*val).remove();
                drop(val);
                bkd.move_prev();
            }
        };

        // remove it from the tree
        unsafe {
            all.cursor_mut_from_ptr(block).remove();
        }

        if block.size_link.is_linked() {
            unsafe {
                free.cursor_mut_from_ptr(block).remove();
            }
        }

        block.num_bytes.set(num_bytes);
        block.start_addr.set(start_addr);

        unsafe {
            all.insert(UnsafeRef::from_raw(block as *const _ as *mut _));
            free.insert(UnsafeRef::from_raw(block as *const _ as *mut _));
        }
    }
}

impl VSpaceManager for Hier {
    type Reservation = Reservation;

    fn map_at_vaddr<A: AllocatorBundle>(
        &self,
        caps: &[seL4_CPtr],
        vaddr: usize,
        size_bits: u8,
        res: &Self::Reservation,
        rights: seL4_CapRights,
        attrs: seL4_ARCH_VMAttributes,
        alloc: &A,
    ) -> bool {
        // vaddr is sign-extended to the full 64-bit range for addresses >= 0x800000000000
        // mask off the high bits to ignore that.
        if (vaddr & (!0 >> (BITS as usize - VADDR_BITS as usize))) < res.res().start_addr.get() ||
            ((vaddr & (!0 >> (BITS as usize - VADDR_BITS as usize))) +
                 (1 << size_bits) * caps.len()) > res.res().end()
        {
            return false;
        }

        // We can't mask off the high bits here because the correct vaddr needs to be sent
        // to the kernel.
        self.map_pages_at_vaddr_raw(caps, vaddr, rights, size_bits, attrs, alloc, false);
        true
    }

    fn change_protection<A: AllocatorBundle>(
        &self,
        vaddr: usize,
        bytes: usize,
        rights: seL4_CapRights,
        attrs: seL4_ARCH_VMAttributes,
        alloc: &A,
    ) {
        self.change_protection_raw(vaddr, bytes, rights, attrs, alloc, false);
    }

    fn unmap(&self, vaddr: usize, bytes: usize, caps: Option<&mut Vec<seL4_CPtr>>) {
        self.unmap_raw(vaddr, bytes, caps, false)
    }

    fn reserve<A: AllocatorBundle>(&self, mut bytes: usize, _: &A) -> Option<Self::Reservation> {
        if bytes % 4096 != 0 {
            bytes += 4096 - bytes % 4096;
        }
        let mut free = self.free_blocks.borrow_mut();
        let mut all = self.all_blocks.borrow_mut();
        let new;
        let opt_leftover;

        {
            let mut best_fit = free.lower_bound_mut(Bound::Included(&bytes));

            match best_fit.get() {
                Some(block) => {
                    if block.num_bytes.get() > bytes {
                        opt_leftover = Some(block.split(bytes));
                        block.num_bytes.set(bytes);
                    } else {
                        opt_leftover = None;
                        assert_eq!(block.num_bytes.get(), bytes);
                    }
                    new = best_fit.remove().unwrap();
                },
                None => return None,
            }
        }

        if let Some(leftover) = opt_leftover {
            free.insert(leftover.clone());
            all.insert(leftover);
        }

        Some(Reservation {
            reservation: new,
        })
    }

    fn reserve_at_vaddr<A: AllocatorBundle>(
        &self,
        vaddr: usize,
        mut bytes: usize,
        _: &A,
    ) -> Option<Self::Reservation> {
        if bytes % 4096 != 0 {
            bytes += 4096 - bytes % 4096;
        }
        // vaddr is sign-extended to the full 64-bit range for addresses > 0x800000000000
        // mask off the high bits to ignore that.
        let vaddr = 4096 * ((vaddr & (!0 >> (BITS as usize - VADDR_BITS as usize))) / 4096);
        let mut all = self.all_blocks.borrow_mut();
        let mut free = self.free_blocks.borrow_mut();
        let new;
        let opt_leftover;
        let opt_newblock;

        {
            // see if the requested range is free.
            let mut containing = all.upper_bound_mut(Bound::Included(&vaddr));

            if !containing.get().unwrap().size_link.is_linked() {
                return None;
            }

            containing.move_next();

            if let Some(c) = containing.get() {
                if c.start_addr.get() <= vaddr + bytes {
                    #[cfg(feature = "debug")]
                    println!("none2");
                    return None;
                }
            }

            containing.move_prev();

            let block = containing.get().unwrap();

            if block.start_addr.get() == vaddr {
                // |       free space       |
                // -----------to-------------
                // | block |    leftover    | (leftover could be empty)
                if block.num_bytes.get() > bytes {
                    opt_leftover = Some(block.split(bytes));
                    block.num_bytes.set(bytes);
                } else {
                    opt_leftover = None;
                    assert_eq!(block.num_bytes.get(), bytes);
                }
                new = unsafe { free.cursor_mut_from_ptr(&*block).remove().unwrap() };
                opt_newblock = None;
            } else {
                // |              free space             |
                // ------------------to-------------------
                // |   block    | newblock |   leftover  | (leftover could be empty)
                let newblock = block.split(vaddr - block.start_addr.get());
                block.num_bytes.set(vaddr - block.start_addr.get());
                if newblock.num_bytes.get() > bytes {
                    opt_leftover = Some(newblock.split(bytes));
                    newblock.num_bytes.set(bytes);
                } else {
                    opt_leftover = None;
                    assert_eq!(newblock.num_bytes.get(), bytes);
                }
                new = newblock.clone();
                opt_newblock = Some(newblock);
            }
        }

        if let Some(newblock) = opt_newblock {
            all.insert(newblock);
        }

        if let Some(leftover) = opt_leftover {
            free.insert(leftover.clone());
            all.insert(leftover);
        }

        Some(Reservation {
            reservation: new,
        })
    }

    fn unreserve<A: AllocatorBundle>(&self, reservation: Self::Reservation, _: &A) {
        self.coalesce(&*reservation.reservation);
    }

    fn unreserve_at_vaddr<A: AllocatorBundle>(&self, vaddr: usize, _: &A) -> Result<(), ()> {
        // vaddr is sign-extended to the full 64-bit range for addresses >= 0x800000000000
        // mask off the high bits to ignore that.
        let vaddr = 4096 * ((vaddr & (!0 >> (BITS as usize - VADDR_BITS as usize))) / 4096);

        let (ptr, is_free) = {
            let mut all = self.all_blocks.borrow_mut();
            let containing = all.upper_bound_mut(Bound::Included(&vaddr));
            let ptr = containing.get().unwrap().into_raw();
            let is_free = containing.get().unwrap().size_link.is_linked();

            (ptr, is_free)
        };

        if !is_free {
            unsafe { self.coalesce(&*ptr) }

            Ok(())
        } else {
            Err(())
        }
    }

    fn get_cap(&self, vaddr: usize) -> Option<seL4_CPtr> {
        match unsafe { (&*self.top_level).get_level_entry(vaddr) } {
            Some(&LevelEntry::Page {
                     cap, ..
                 }) => Some(cap),
            _ => None,
        }
    }

    fn root(&self) -> seL4_CPtr {
        unsafe { (*self.top_level).table_cap }
    }

    fn minimum_slots(&self) -> usize {
        // conservative - might need a handful of caps when creating deepl
        32
    }

    fn minimum_untyped(&self) -> usize {
        // conservative - 16KiB per hierarchy level is more than enough for storing paging
        // structures.
        HIER_LEVELS.len() * 16 * 1024
    }

    fn minimum_vspace(&self) -> usize {
        // conservative - 16KiB per hierarchy level is more than enough for storing LevelNodes.
        HIER_LEVELS.len() * 16 * 1024
    }
}
