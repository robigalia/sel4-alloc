// Copyright 2016 Robigalia Project Developers
//
// Licensed under the Apache License, Version 2.0, <LICENSE-APACHE or
// http://apache.org/licenses/LICENSE-2.0> or the MIT license <LICENSE-MIT or
// http://opensource.org/licenses/MIT>, at your option. This file may not be
// copied, modified, or distributed except according to those terms.

use core::ops;

use sel4::{SlotRef, Window, CNodeInfo, CNode};
use sel4_sys::{seL4_CPtr, seL4_CapInitThreadVSpace, seL4_CapRights};

use cspace::BumpAllocator;
use utspace::UtBucket;
use vspace::Hier;

use {VSpaceManager, VSpaceReservation};

extern "C" {
    static __executable_start: u8;
}

#[cfg(target_pointer_width = "32")]
mod consts {
    pub const BITS: u8 = 32;

    pub const LEN_SEG_HDR: isize = 0x2a;
    pub const OFFSET_SEG_HDR: isize = 0x1c;
    pub const NUM_SEG_HDRS: isize = 0x2c;

    pub const VADDR_START: isize = 0x08;
    pub const VADDR_END: isize = 0x14;
}

#[cfg(target_pointer_width = "64")]
mod consts {
    pub const BITS: u8 = 64;

    pub const LEN_SEG_HDR: isize = 0x36;
    pub const OFFSET_SEG_HDR: isize = 0x20;
    pub const NUM_SEG_HDRS: isize = 0x38;

    pub const VADDR_START: isize = 0x10;
    pub const VADDR_END: isize = 0x28;
}

/// Accessor for resources needed to bootstrap an allocator stack.
pub trait BootstrapResources {
    fn untyped(&self) -> (SlotRef, u8);

    fn cnode(&self) -> (Window, CNodeInfo);

    fn vspace(&self) -> seL4_CPtr;

    fn existing_mappings(&self) -> Option<(ops::Range<seL4_CPtr>, ops::Range<usize>)>;

    fn stack_guard_page(&self) -> Option<usize>;
}

impl BootstrapResources for ::sel4_sys::seL4_BootInfo {
    fn untyped(&self) -> (SlotRef, u8) {
        (
            SlotRef {
                root: CNode::from_cap(0x2),
                cptr: self.untyped.start,
                depth: consts::BITS,
            },
            unsafe { self.untyped_descs()[0].sizeBits },
        )
    }

    fn cnode(&self) -> (Window, CNodeInfo) {
        #[cfg(feature = "debug")]
        println!("root has {} as init cnode size bits", self.initThreadCNodeSizeBits);

        (
            Window {
                cnode: SlotRef {
                    root: CNode::from_cap(0x2),
                    cptr: 2,
                    depth: consts::BITS,
                },
                first_slot_idx: self.empty.start,
                num_slots: self.empty.end - self.empty.start,
            },
            CNodeInfo {
                guard_val: 0,
                radix_bits: self.initThreadCNodeSizeBits,
                guard_bits: consts::BITS - self.initThreadCNodeSizeBits,
                prefix_bits: 0,
            },
        )
    }

    fn vspace(&self) -> seL4_CPtr {
        seL4_CapInitThreadVSpace
    }

    fn existing_mappings(&self) -> Option<(ops::Range<seL4_CPtr>, ops::Range<usize>)> {
        Some((
            self.userImageFrames.start..self.userImageFrames.end,
            get_vaddr_range_from_elf_hdr(unsafe { &__executable_start }),
        ))
    }

    // This should probably be refactored somewhere else in order to remove the dependency on
    // sel4-start, once this library starts being used by apps that aren't an seL4 root task.
    fn stack_guard_page(&self) -> Option<usize> {
        let mut addr = ::sel4_start::get_stack_bottom_addr();

        if addr % 4096 != 0 {
            addr += 4096 - addr % 4096;
        }

        Some(addr)
    }
}

/// Bootstrap allocators from initial resources to enable an actual allocator to be able to run.
///
/// # Note
///
/// This requires `alloc` functions to work! The recommended implementation is a simple bump
/// allocator using memory defined in the ELF section.
pub fn bootstrap_allocators<B: BootstrapResources>(bi: &B) -> (BumpAllocator, UtBucket, Hier) {
    let cnode = bi.cnode();
    let bump = BumpAllocator::new(cnode.0, cnode.1);

    let ut = bi.untyped();
    let bucket = UtBucket::new(ut.0, ut.1);

    let alloc = (bump, bucket, ::DummyAlloc);
    let hier = Hier::new(&alloc, Some(bi.vspace()));

    if let Some((cptrs, vaddrs)) = bi.existing_mappings() {
        let rsvp = hier.reserve_at_vaddr(vaddrs.start, vaddrs.end - vaddrs.start, &alloc)
            .unwrap();
        let mut addr = vaddrs.start;

        for cptr in cptrs {
            let res = hier.mock_map_at_vaddr(
                &[cptr],
                addr,
                12,
                &rsvp,
                seL4_CapRights::new(0, 1, 1),
                Default::default(),
                &alloc,
            );
            addr += 4096;
            assert!(res);
        }

        if vaddrs.end % 4096 != 0 {
            assert_eq!(addr, vaddrs.end + (4096 - vaddrs.end % 4096));
        } else {
            assert_eq!(addr, vaddrs.end);
        }

        assert_eq!(addr, rsvp.end_vaddr());
    }

    if let Some(addr) = bi.stack_guard_page() {
        hier.change_protection(
            addr,
            4096,
            seL4_CapRights::new(0, 0, 0),
            Default::default(),
            &alloc,
        );
    }

    // Reserve the first 2MB of virtual memory to avoid handing that out to applications
    // Applications that want to map this memory can call unreserve_at_vaddr(0).
    let _ = hier.reserve_at_vaddr(0, 1024 * 1024 * 2, &alloc);

    (alloc.0, alloc.1, hier)
}

// Enumerate the elf headers to determine our starting and ending vaddrs
fn get_vaddr_range_from_elf_hdr(header: *const u8) -> ops::Range<usize> {
    unsafe {
        assert_eq!(*(header as *const u32), 0x464c457f);

        let len_seg_hdr = *(header.offset(consts::LEN_SEG_HDR) as *const u16);
        let offset_seg_hdr = *(header.offset(consts::OFFSET_SEG_HDR) as *const isize);
        let num_seg_hdrs = *(header.offset(consts::NUM_SEG_HDRS) as *const u16);

        let mut segment = header.offset(offset_seg_hdr) as *const u8;
        let mut low_vaddr = usize::max_value();
        let mut high_vaddr = 0;

        for _ in 0..num_seg_hdrs {
            let vaddr_start = *(segment.offset(consts::VADDR_START) as *const usize);
            let vaddr_end = vaddr_start + *(segment.offset(consts::VADDR_END) as *const usize);

            if vaddr_start > 0 {
                if vaddr_start < low_vaddr {
                    low_vaddr = vaddr_start;
                }
                if vaddr_end > high_vaddr {
                    high_vaddr = vaddr_end;
                }
            }

            segment = segment.offset(len_seg_hdr as isize) as *const u8;
        }

        low_vaddr..high_vaddr
    }
}
